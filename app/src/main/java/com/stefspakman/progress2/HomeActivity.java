package com.stefspakman.progress2;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.stefspakman.progress2.Download_data.download_complete;
import android.util.Log;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HomeActivity extends AppBaseActivity implements download_complete {

    private ProgressDBHelper dbHelper ;
    SharedPreferences getData;
    TextView userName;
    TextView studyYear;
    TextView studyCurrentETCS;
    TextView studyAdvice;
    TextView studyKernVakken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        userName = (TextView) findViewById(R.id.displayName2);
        studyYear = (TextView) findViewById(R.id.displayYear2);
        studyCurrentETCS = (TextView) findViewById(R.id.displayCurrentETCS);
        studyAdvice = (TextView) findViewById(R.id.displayStudyadvice);

        studyKernVakken = (TextView) findViewById(R.id.displayKernVakken);

        Download_data download_data = new Download_data((download_complete) this);
        download_data.download_data_from_link("http://test.stefspakman.com/courses.json");


        dbHelper = new ProgressDBHelper(this);


        getSettings();
        setSettings();

    }
    @Override
    protected void onResume() {
        super.onResume();
        getSettings();
        setSettings();
    }

    private void getSettings() {
        getData = getSharedPreferences("com.stefspakman.progress2_preferences", Context.MODE_PRIVATE);
        String userText = getData.getString("user_name", getString(R.string.no_name));
        userName.setText(userText);
    }

    private void setSettings() {
        studyYear.setText(getString(R.string.studyyear));
        String totalPoints = Integer.toString(dbHelper.getTotalECTS());
        studyCurrentETCS.setText(totalPoints);

        String maxPoints = Integer.toString(dbHelper.getMaxECTS());
        String maxAvailablePoints = Integer.toString(dbHelper.getMaxAvailableECTS());
        String missedPoints = Integer.toString(dbHelper.getMissedECTS());

        String onbekend = getString(R.string.unknown_grade);
        String IOPR1 = dbHelper.getIOPR1();
        Double IOPR1grade;
        if(IOPR1.equals(onbekend)){
            IOPR1grade = 0.0;
        }
        else {
            IOPR1grade = Double.parseDouble(IOPR1);
            Log.d("Progressmessage", Double.toString(IOPR1grade));
        }

        String IRDB = dbHelper.getIRDB();
        Double IRDBgrade;
        if(IRDB.equals(onbekend)){
            IRDBgrade = 0.0;
        }
        else {
            IRDBgrade = Double.parseDouble(IRDB);
            Log.d("Progressmessage", Double.toString(IRDBgrade));
        }
        String INET = dbHelper.getINET();
        Double INETgrade;
        if(INET.equals(onbekend)){
            INETgrade = 0.0;
        }
        else {
            INETgrade = Double.parseDouble(INET);
            Log.d("Progressmessage", Double.toString(INETgrade));
        }
        String IOPR2 = dbHelper.getIOPR2();
        Double IOPR2grade;
        if(IOPR2.equals(onbekend)){
            IOPR2grade = 0.0;
        }
        else {
            IOPR2grade = Double.parseDouble(IOPR2);
            Log.d("Progressmessage", Double.toString(IOPR2grade));
        }

        int kernVakken = 0;
        int kernVakkenToDo = 0;
        int nietGehaaldeKernVakken = 0;
        Double waardeNB = 0.0;
        Double waardeGehaald = 5.5;

        if(waardeGehaald == 10){
            kernVakken++;
        }

        //IOPR1
        if (IOPR1grade == waardeNB){
            kernVakkenToDo++;
        }
        else if (IOPR1grade <waardeGehaald && IOPR1grade != waardeNB){
            nietGehaaldeKernVakken++;
        }
        else if (IOPR1grade >= waardeGehaald){
            kernVakken++;
        }
        //IRDB
        if (IRDBgrade == waardeNB){
            kernVakkenToDo++;
        }
        else if (IRDBgrade <waardeGehaald && IRDBgrade != waardeNB){
            nietGehaaldeKernVakken++;
        }
        else if (IRDBgrade >= waardeGehaald){
            kernVakken++;
        }
        //INET
        if (INETgrade == waardeNB){
            kernVakkenToDo++;
        }
        else if (INETgrade <waardeGehaald && INETgrade != waardeNB){
            nietGehaaldeKernVakken++;
        }
        else if (INETgrade >= waardeGehaald){
            kernVakken++;
        }
        //IOPR2
        if (IOPR2grade == waardeNB){
            kernVakkenToDo++;
        }
        else if (IOPR2grade <waardeGehaald && IOPR2grade != waardeNB){
            nietGehaaldeKernVakken++;
        }
        else if (IOPR2grade >= waardeGehaald){
            kernVakken++;
        }

        int pointCounter = dbHelper.getTotalECTS();
        int availablePoints = dbHelper.getMaxAvailableECTS();
        if (pointCounter == 0 && nietGehaaldeKernVakken == 0){
            studyAdvice.setText(getString(R.string.no_grades_submitted));
        }
        else if (pointCounter == 60){
            if (kernVakken < 3 && nietGehaaldeKernVakken > 1){
                studyAdvice.setText(getString(R.string.no_core_courses));
            }
            else {
                studyAdvice.setText(getString(R.string.all_ects));
            }
        }
        else if (pointCounter >= 50){
            if (kernVakken < 3 && nietGehaaldeKernVakken > 1){
                studyAdvice.setText(getString(R.string.no_core_courses));
            }
            else {
                studyAdvice.setText(getString(R.string.till_next_year));
            }
        }
        else if (
                pointCounter == 0 && availablePoints < 40 ||
                pointCounter == 1 && availablePoints < 39 ||
                pointCounter == 2 && availablePoints < 38 ||
                pointCounter == 3 && availablePoints < 37 ||
                pointCounter == 4 && availablePoints < 36 ||
                pointCounter == 5 && availablePoints < 35 ||
                pointCounter == 6 && availablePoints < 34 ||
                pointCounter == 7 && availablePoints < 33 ||
                pointCounter == 8 && availablePoints < 32 ||
                pointCounter == 9 && availablePoints < 31 ||
                pointCounter == 10 && availablePoints < 30 ||
                pointCounter == 11 && availablePoints < 29 ||
                pointCounter == 12 && availablePoints < 28 ||
                pointCounter == 13 && availablePoints < 27 ||
                pointCounter == 14 && availablePoints < 26 ||
                pointCounter == 15 && availablePoints < 25 ||
                pointCounter == 16 && availablePoints < 24 ||
                pointCounter == 17 && availablePoints < 23 ||
                pointCounter == 18 && availablePoints < 22 ||
                pointCounter == 19 && availablePoints < 21 ||
                pointCounter == 20 && availablePoints < 20 ||
                pointCounter == 21 && availablePoints < 19 ||
                pointCounter == 22 && availablePoints < 18 ||
                pointCounter == 23 && availablePoints < 17 ||
                pointCounter == 24 && availablePoints < 16 ||
                pointCounter == 25 && availablePoints < 15 ||
                pointCounter == 26 && availablePoints < 14 ||
                pointCounter == 27 && availablePoints < 13 ||
                pointCounter == 28 && availablePoints < 12 ||
                pointCounter == 29 && availablePoints < 11 ||
                pointCounter == 30 && availablePoints < 10 ||
                pointCounter == 31 && availablePoints < 9 ||
                pointCounter == 32 && availablePoints < 8 ||
                pointCounter == 33 && availablePoints < 7 ||
                pointCounter == 34 && availablePoints < 6 ||
                pointCounter == 35 && availablePoints < 5 ||
                pointCounter == 36 && availablePoints < 4 ||
                pointCounter == 37 && availablePoints < 3 ||
                pointCounter == 38 && availablePoints < 2 ||
                pointCounter == 39 && availablePoints < 1
                ){
            studyAdvice.setText(getString(R.string.you_do_not_pass));
        }

        //Je blijft zitten
        else if (
                pointCounter == 40 && availablePoints < 10 ||
                pointCounter == 41 && availablePoints < 9 ||
                pointCounter == 42 && availablePoints < 8 ||
                pointCounter == 43 && availablePoints < 7 ||
                pointCounter == 44 && availablePoints < 6 ||
                pointCounter == 45 && availablePoints < 5 ||
                pointCounter == 46 && availablePoints < 4 ||
                pointCounter == 47 && availablePoints < 3 ||
                pointCounter == 48 && availablePoints < 2 ||
                pointCounter == 49 && availablePoints < 1
                ){
            if (kernVakken < 3 && nietGehaaldeKernVakken > 1){
                studyAdvice.setText(getString(R.string.no_core_courses));
            }
            else {
                studyAdvice.setText(getString(R.string.you_stay_put));
            }
        }

        //Je kan nog door
        else if (
                pointCounter == 0 && availablePoints >= 50 ||
                pointCounter == 1 && availablePoints >= 49 ||
                pointCounter == 2 && availablePoints >= 48 ||
                pointCounter == 3 && availablePoints >= 47 ||
                pointCounter == 4 && availablePoints >= 46 ||
                pointCounter == 5 && availablePoints >= 45 ||
                pointCounter == 6 && availablePoints >= 44 ||
                pointCounter == 7 && availablePoints >= 43 ||
                pointCounter == 8 && availablePoints >= 42 ||
                pointCounter == 9 && availablePoints >= 41 ||
                pointCounter == 10 && availablePoints >= 40 ||
                pointCounter == 11 && availablePoints >= 39 ||
                pointCounter == 12 && availablePoints >= 38 ||
                pointCounter == 13 && availablePoints >= 37 ||
                pointCounter == 14 && availablePoints >= 36 ||
                pointCounter == 15 && availablePoints >= 35 ||
                pointCounter == 16 && availablePoints >= 34 ||
                pointCounter == 17 && availablePoints >= 33 ||
                pointCounter == 18 && availablePoints >= 32 ||
                pointCounter == 19 && availablePoints >= 31 ||
                pointCounter == 20 && availablePoints >= 30 ||
                pointCounter == 21 && availablePoints >= 29 ||
                pointCounter == 22 && availablePoints >= 28 ||
                pointCounter == 23 && availablePoints >= 27 ||
                pointCounter == 24 && availablePoints >= 26 ||
                pointCounter == 25 && availablePoints >= 25 ||
                pointCounter == 26 && availablePoints >= 24 ||
                pointCounter == 27 && availablePoints >= 23 ||
                pointCounter == 28 && availablePoints >= 22 ||
                pointCounter == 29 && availablePoints >= 21 ||
                pointCounter == 30 && availablePoints >= 20 ||
                pointCounter == 31 && availablePoints >= 19 ||
                pointCounter == 32 && availablePoints >= 18 ||
                pointCounter == 33 && availablePoints >= 17 ||
                pointCounter == 34 && availablePoints >= 16 ||
                pointCounter == 35 && availablePoints >= 15 ||
                pointCounter == 36 && availablePoints >= 14 ||
                pointCounter == 37 && availablePoints >= 13 ||
                pointCounter == 38 && availablePoints >= 12 ||
                pointCounter == 39 && availablePoints >= 11 ||
                pointCounter == 40 && availablePoints >= 10 ||
                pointCounter == 41 && availablePoints >= 9 ||
                pointCounter == 42 && availablePoints >= 8 ||
                pointCounter == 43 && availablePoints >= 7 ||
                pointCounter == 44 && availablePoints >= 6 ||
                pointCounter == 45 && availablePoints >= 5 ||
                pointCounter == 46 && availablePoints >= 4 ||
                pointCounter == 47 && availablePoints >= 3 ||
                pointCounter == 48 && availablePoints >= 2 ||
                pointCounter == 49 && availablePoints >= 1
                ){
            if (kernVakken < 3 && nietGehaaldeKernVakken > 1){
                studyAdvice.setText(getString(R.string.no_core_courses));
            }
            else {
                studyAdvice.setText(getString(R.string.still_chance));
            }
        }

        //Je kan nog blijven zitten
        else if (
                pointCounter == 0 && availablePoints >= 40 ||
                pointCounter == 1 && availablePoints >= 39 ||
                pointCounter == 2 && availablePoints >= 38 ||
                pointCounter == 3 && availablePoints >= 37 ||
                pointCounter == 4 && availablePoints >= 36 ||
                pointCounter == 5 && availablePoints >= 35 ||
                pointCounter == 6 && availablePoints >= 34 ||
                pointCounter == 7 && availablePoints >= 33 ||
                pointCounter == 8 && availablePoints >= 32 ||
                pointCounter == 9 && availablePoints >= 31 ||
                pointCounter == 10 && availablePoints >= 30 ||
                pointCounter == 11 && availablePoints >= 29 ||
                pointCounter == 12 && availablePoints >= 28 ||
                pointCounter == 13 && availablePoints >= 27 ||
                pointCounter == 14 && availablePoints >= 26 ||
                pointCounter == 15 && availablePoints >= 25 ||
                pointCounter == 16 && availablePoints >= 24 ||
                pointCounter == 17 && availablePoints >= 23 ||
                pointCounter == 18 && availablePoints >= 22 ||
                pointCounter == 19 && availablePoints >= 21 ||
                pointCounter == 20 && availablePoints >= 20 ||
                pointCounter == 21 && availablePoints >= 19 ||
                pointCounter == 22 && availablePoints >= 18 ||
                pointCounter == 23 && availablePoints >= 17 ||
                pointCounter == 24 && availablePoints >= 16 ||
                pointCounter == 25 && availablePoints >= 15 ||
                pointCounter == 26 && availablePoints >= 14 ||
                pointCounter == 27 && availablePoints >= 13 ||
                pointCounter == 28 && availablePoints >= 12 ||
                pointCounter == 29 && availablePoints >= 11 ||
                pointCounter == 30 && availablePoints >= 10 ||
                pointCounter == 31 && availablePoints >= 9 ||
                pointCounter == 32 && availablePoints >= 8 ||
                pointCounter == 33 && availablePoints >= 7 ||
                pointCounter == 34 && availablePoints >= 6 ||
                pointCounter == 35 && availablePoints >= 5 ||
                pointCounter == 36 && availablePoints >= 4 ||
                pointCounter == 37 && availablePoints >= 3 ||
                pointCounter == 38 && availablePoints >= 2 ||
                pointCounter == 39 && availablePoints >= 1
                ){
            if (kernVakken < 3 && nietGehaaldeKernVakken > 1){
                studyAdvice.setText(getString(R.string.no_core_courses));
            }
            else {
                studyAdvice.setText(getString(R.string.chance_to_stay));
            }
        }
    }

    public void get_data(String data)    {
        try {
            JSONArray data_array=new JSONArray(data);

            for (int i = 0 ; i < data_array.length() ; i++)
            {
                JSONObject obj=new JSONObject(data_array.get(i).toString());

                String courseEditText = obj.getString("name");
                String ectsEditText = obj.getString("ects");
                String periodEditText = obj.getString("period");
                String gradeEditText = obj.getString("grade");

                dbHelper.insertCourse(courseEditText.toString(),
                        Integer.parseInt(ectsEditText.toString()),
                        Integer.parseInt(periodEditText.toString()),
                        Integer.parseInt(gradeEditText.toString()));

                Log.d("ProgressMessage", obj.getString("name"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}