package com.stefspakman.progress2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by stefspakman on 05-Apr-16.
 */
public class ProgressDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "SQLiteProgress.db";
    private static final int DATABASE_VERSION = 1;
    public static final String COURSE_TABLE_NAME = "course";
    public static final String COURSE_COLUMN_ID = "_id";
    public static final String COURSE_COLUMN_COURSE = "course";
    public static final String COURSE_COLUMN_ECTS = "ects";
    public static final String COURSE_COLUMN_PERIOD = "period";
    public static final String COURSE_COLUMN_GRADE = "grade";


    public ProgressDBHelper(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + COURSE_TABLE_NAME + "(" +
                        COURSE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        COURSE_COLUMN_COURSE + " VARCHAR(10) UNIQUE, " +
                        COURSE_COLUMN_ECTS + " INTEGER, " +
                        COURSE_COLUMN_PERIOD + " INTEGER, " +
                        COURSE_COLUMN_GRADE + " VARCHAR)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public boolean insertCourse(String course, int ects, int period, int grade) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COURSE_COLUMN_COURSE, course);
        contentValues.put(COURSE_COLUMN_ECTS, ects);
        contentValues.put(COURSE_COLUMN_PERIOD, period);
        contentValues.put(COURSE_COLUMN_GRADE, "nb");

        db.insert(COURSE_TABLE_NAME, null, contentValues);
        Log.d("Progressmessage", "DBUpdate");
        return true;
    }

    public boolean updateCourse(Integer id, String course, int ects, int period, double grade) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COURSE_COLUMN_COURSE, course);
        contentValues.put(COURSE_COLUMN_ECTS, ects);
        contentValues.put(COURSE_COLUMN_PERIOD, period);
        contentValues.put(COURSE_COLUMN_GRADE, grade);
        db.update(COURSE_TABLE_NAME, contentValues, COURSE_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public Cursor getCourse(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery( "SELECT * FROM " + COURSE_TABLE_NAME + " WHERE " +
                COURSE_COLUMN_ID + "=?", new String[] { Integer.toString(id) } );
        return res;
    }

    public Cursor getAllCourses() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery( "SELECT * FROM " + COURSE_TABLE_NAME, null );
        return res;
    }

    public int getTotalECTS() {
        int totalPoints = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT SUM(" + COURSE_COLUMN_ECTS + ") FROM " + COURSE_TABLE_NAME + " WHERE " + COURSE_COLUMN_GRADE + ">=5.5 AND " + COURSE_COLUMN_GRADE + "!='nb' OR " + COURSE_COLUMN_GRADE + "  ==10.0 AND " + COURSE_COLUMN_GRADE + "!='nb'", null);
        if(cur.moveToFirst())
        {
            return cur.getInt(0);
        }

        return totalPoints;
    }//Krijg het aantal behaalde punten

    public int getMaxECTS() {
        int maxPoints = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT SUM(" + COURSE_COLUMN_ECTS + ") FROM " + COURSE_TABLE_NAME, null);
        if(cur.moveToFirst())
        {
            return cur.getInt(0);
        }

        return maxPoints;
    }//Krijg het maximaal aantal punten (60)
    public int getMaxAvailableECTS() {
        int maxAvailablePoints = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT SUM(" + COURSE_COLUMN_ECTS + ") FROM " + COURSE_TABLE_NAME + " WHERE " + COURSE_COLUMN_GRADE + "='nb'", null);
        if(cur.moveToFirst())
        {
            return cur.getInt(0);
        }

        return maxAvailablePoints;
    }//Krijg de nog beschikbare ECTS (nog beschikbare vakken)
    public int getMissedECTS() {
        int missedPoints = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT SUM(" + COURSE_COLUMN_ECTS + ") FROM " + COURSE_TABLE_NAME + " WHERE " + COURSE_COLUMN_GRADE + " < 5.5 AND " + COURSE_COLUMN_GRADE + " <> 10.0", null);
        if(cur.moveToFirst())
        {
            return cur.getInt(0);
        }

        return missedPoints;
    }//Krijg de misgelopen ECTS
    public String getIOPR1() {
        String IOPR1 = "0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + COURSE_COLUMN_GRADE + " FROM " + COURSE_TABLE_NAME + " WHERE " + COURSE_COLUMN_COURSE + "='IOPR1'", null);
        if(cur.moveToFirst())
        {
            return cur.getString(0);
        }

        return IOPR1;
    }//Krijg Cijfer van IOPR1
    public String getIRDB() {
        String IRDB = "0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + COURSE_COLUMN_GRADE + " FROM " + COURSE_TABLE_NAME + " WHERE " + COURSE_COLUMN_COURSE + "='IRDB'", null);
        if(cur.moveToFirst())
        {
            Log.d("Progressmessage",cur.getString(0));
            return cur.getString(0);
        }

        return IRDB;
    }//Krijg Cijfer van IRDB
    public String getINET() {
        String INET = "0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + COURSE_COLUMN_GRADE + " FROM " + COURSE_TABLE_NAME + " WHERE " + COURSE_COLUMN_COURSE + "='INET'", null);
        if(cur.moveToFirst())
        {
            return cur.getString(0);
        }

        return INET;
    }//Krijg Cijfer van IIBPM
    public String getIOPR2() {
        String IOPR2 = "0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + COURSE_COLUMN_GRADE + " FROM " + COURSE_TABLE_NAME + " WHERE " + COURSE_COLUMN_COURSE + "='IOPR2'", null);
        if(cur.moveToFirst())
        {
            return cur.getString(0);
        }

        return IOPR2;
    }//Krijg Cijfer van IOPR2

    public Integer deleteCourse(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(COURSE_TABLE_NAME,
                COURSE_COLUMN_ID + " = ? ",
                new String[] { Integer.toString(id) });
    }
}
