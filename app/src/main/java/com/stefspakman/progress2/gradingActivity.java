package com.stefspakman.progress2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import com.stefspakman.progress2.Download_data.download_complete;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class gradingActivity extends AppBaseActivity implements download_complete {

    public final static String KEY_EXTRA_CONTACT_ID = "KEY_EXTRA_CONTACT_ID";
    private ProgressDBHelper dbHelper ;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grading);

//        Button button = (Button) findViewById(R.id.addNew);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(gradingActivity.this, CreateOrEditDBInfoActivity.class);
//                intent.putExtra(KEY_EXTRA_CONTACT_ID, 0);
//                startActivity(intent);
//            }
//        });

        Download_data download_data = new Download_data((download_complete) this);
        download_data.download_data_from_link("http://test.stefspakman.com/courses.json");


        dbHelper = new ProgressDBHelper(this);


        final Cursor cursor = dbHelper.getAllCourses();
        String [] columns = new String[] {
                // ProgressDBHelper.COURSE_COLUMN_ID,
                ProgressDBHelper.COURSE_COLUMN_COURSE,
                ProgressDBHelper.COURSE_COLUMN_ECTS,
                ProgressDBHelper.COURSE_COLUMN_PERIOD,
                ProgressDBHelper.COURSE_COLUMN_GRADE
        };
        int [] widgets = new int[] {
                //R.id.courseID,
                R.id.courseName,
                R.id.courseEcts,
                R.id.coursePeriod,
                R.id.courseGrade
        };

        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this, R.layout.courses_db_info,
                cursor, columns, widgets, 0);
        listView = (ListView)findViewById(R.id.listView1);
        listView.setAdapter(cursorAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView listView, View view,
                                    int position, long id) {
                Cursor itemCursor = (Cursor) gradingActivity.this.listView.getItemAtPosition(position);
                int courseID = itemCursor.getInt(itemCursor.getColumnIndex(ProgressDBHelper.COURSE_COLUMN_ID));
                Intent intent = new Intent(getApplicationContext(), CreateOrEditDBInfoActivity.class);
                intent.putExtra(KEY_EXTRA_CONTACT_ID, courseID);
                startActivity(intent);
            }
        });
    }

    public void get_data(String data)    {
        try {
            JSONArray data_array=new JSONArray(data);

            for (int i = 0 ; i < data_array.length() ; i++)
            {
                JSONObject obj=new JSONObject(data_array.get(i).toString());

                String courseEditText = obj.getString("name");
                String ectsEditText = obj.getString("ects");
                String periodEditText = obj.getString("grade");
                String gradeEditText = obj.getString("period");

                dbHelper.insertCourse(courseEditText.toString(),
                        Integer.parseInt(ectsEditText.toString()),
                        Integer.parseInt(periodEditText.toString()),
                        Integer.parseInt(gradeEditText.toString()));

                Log.d("ProgressMessage", obj.getString("name"));

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}