package com.stefspakman.progress2;

/**
 * Created by stefspakman on 05-Apr-16.
 */
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class CreateOrEditDBInfoActivity extends AppBaseActivity implements View.OnClickListener {

    private ProgressDBHelper dbHelper ;
    EditText courseEditText;
    EditText ectsEditText;
    EditText periodEditText;
    EditText gradeEditText;

    Button saveButton;
    LinearLayout buttonLayout;
    Button editButton;

    int courseID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        courseID = getIntent().getIntExtra(gradingActivity.KEY_EXTRA_CONTACT_ID, 0);

        setContentView(R.layout.activity_edit_db);
        courseEditText = (EditText) findViewById(R.id.editTextCourse);
        ectsEditText = (EditText) findViewById(R.id.editTextEcts);
        periodEditText = (EditText) findViewById(R.id.editTextPeriod);
        gradeEditText = (EditText) findViewById(R.id.editTextGrade);

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setOnClickListener(this);
        buttonLayout = (LinearLayout) findViewById(R.id.buttonLayout);
        editButton = (Button) findViewById(R.id.editButton);
        editButton.setOnClickListener(this);

        dbHelper = new ProgressDBHelper(this);

        if(courseID > 0) {
            saveButton.setVisibility(View.GONE);
            buttonLayout.setVisibility(View.VISIBLE);

            Cursor rs = dbHelper.getCourse(courseID);
            rs.moveToFirst();
            String courseName = rs.getString(rs.getColumnIndex(ProgressDBHelper.COURSE_COLUMN_COURSE));
            int courseECTS = rs.getInt(rs.getColumnIndex(ProgressDBHelper.COURSE_COLUMN_ECTS));
            int coursePeriod = rs.getInt(rs.getColumnIndex(ProgressDBHelper.COURSE_COLUMN_PERIOD));
            double courseGrade = rs.getDouble(rs.getColumnIndex(ProgressDBHelper.COURSE_COLUMN_GRADE));
            if (!rs.isClosed()) {
                rs.close();
            }

            courseEditText.setText(courseName);
            courseEditText.setFocusable(false);
            courseEditText.setClickable(false);

            ectsEditText.setText((CharSequence) (courseECTS + ""));
            ectsEditText.setFocusable(false);
            ectsEditText.setClickable(false);

            periodEditText.setText((CharSequence) (coursePeriod + ""));
            periodEditText.setFocusable(false);
            periodEditText.setClickable(false);

            gradeEditText.setText((CharSequence) (courseGrade + ""));
            gradeEditText.setFocusable(false);
            gradeEditText.setClickable(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.saveButton:
                persistCourse();
                return;
            case R.id.editButton:
                saveButton.setVisibility(View.VISIBLE);
                buttonLayout.setVisibility(View.GONE);
                courseEditText.setEnabled(false);
                courseEditText.setFocusableInTouchMode(false);
                courseEditText.setClickable(false);

                ectsEditText.setEnabled(false);
                ectsEditText.setFocusableInTouchMode(false);
                ectsEditText.setClickable(false);

                periodEditText.setEnabled(false);
                periodEditText.setFocusableInTouchMode(false);
                periodEditText.setClickable(false);

                gradeEditText.setEnabled(true);
                gradeEditText.setFocusableInTouchMode(true);
                gradeEditText.setClickable(true);
                return;
        }
    }

    public void persistCourse() {
        if(courseID > 0) {
            if(dbHelper.updateCourse(courseID, courseEditText.getText().toString(),
                    Integer.parseInt(ectsEditText.getText().toString()),
                    Integer.parseInt(periodEditText.getText().toString()),
                    Double.parseDouble(gradeEditText.getText().toString()))) {
                Toast.makeText(getApplicationContext(), getString(R.string.toast_course_update_succesfull), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), gradingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            else {
                Toast.makeText(getApplicationContext(), getString(R.string.toast_course_update_unsuccesfull), Toast.LENGTH_SHORT).show();
            }
        }
        else {
            if(dbHelper.insertCourse(courseEditText.getText().toString(),
                    Integer.parseInt(ectsEditText.getText().toString()),
                    Integer.parseInt(periodEditText.getText().toString()),
                    Integer.parseInt(gradeEditText.getText().toString()))) {
                Toast.makeText(getApplicationContext(), getString(R.string.toast_course_added), Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getApplicationContext(), getString(R.string.toast_course_not_added), Toast.LENGTH_SHORT).show();
            }
            Intent intent = new Intent(getApplicationContext(), gradingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }
}
